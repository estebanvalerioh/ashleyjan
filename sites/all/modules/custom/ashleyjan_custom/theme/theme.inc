<?php
/**
 * @file
 * AshleyJan custom theme functions
 */

/*
 * Create homepage slideshow
 */
function theme_ashleyjan_custom_featuredworks($works) {
  $output = <<<EOD
<div class="carouselslider featured_posts items3" data-count="3">
  <ul class="item_list">
    <li>
      <div class="item">
        <div class="img_block">
          <img src="img/pictures/carousel_works1.jpg" alt="" width="270" height="190">
          <div class="ico_gallery"></div>
          <div class="carousel_fadder"></div>
        </div>
        <div class="carousel_body">
          <div class="post_type_ico">
            <span class="post_type_video"></span>
          </div><!-- .post type -->
          <div class="carousel_title">
            <h6><a href="#">Lorem ipsum dolor</a></h6>
          </div>
          <div class="carousel_desc">
            <div class="exc">Consectetur adipiscing elit ac Praesent laoreet rutrum suada duis ac nisi, ac sapiens ccumsan elementum acnullam</div>
          </div>
          <div class="carousel_meta"><span>25 April 2019 in</span> <a href="#">Portraits,</a> <a href="#">Fashion</a></div>
        </div>
      </div>
    </li>

    <li>
      <div class="item">
        <div class="img_block">
          <img src="img/pictures/carousel_works2.jpg" alt="" width="270" height="190">
          <div class="ico_gallery"></div>
          <div class="carousel_fadder"></div>
        </div>
        <div class="carousel_body">
          <div class="post_type_ico">
            <span class="post_type_image"></span>
          </div><!-- .post type -->
          <div class="carousel_title">
            <h6><a href="#">Dolor sit amet</a></h6>
          </div>
          <div class="carousel_desc">
            <div class="exc">Nunc justo lacus, molestie ut porta id pharetra vestibulum proin sit amet aliquet massa phasellus id tristique mi</div>
          </div>
          <div class="carousel_meta"><span>25 April 2019 in</span> <a href="#">Portraits,</a> <a href="#">Fashion</a></div>
        </div>
      </div>
    </li>

    <li>
      <div class="item">
        <div class="img_block">
          <img src="img/pictures/carousel_works3.jpg" alt="" width="270" height="190">
          <div class="ico_gallery"></div>
          <div class="carousel_fadder"></div>
        </div>
        <div class="carousel_body">
          <div class="post_type_ico">
            <span class="post_type_gallery"></span>
          </div><!-- .post type -->
          <div class="carousel_title">
            <h6><a href="#">Nunc justo lacus</a></h6>
          </div>
          <div class="carousel_desc">
            <div class="exc">Consectetur adipiscing elit ac Praesent laoreet rutrum suada duis ac nisi, ac sapiens ccumsan elementum acnullam</div>
          </div>
          <div class="carousel_meta"><span>25 April 2019 in</span> <a href="#">Portraits,</a> <a href="#">Fashion</a></div>
        </div>
      </div>
    </li>

    <li>
      <div class="item">
        <div class="img_block">
          <img src="img/pictures/carousel_works4.jpg" alt="" width="270" height="190">
          <div class="ico_gallery"></div>
          <div class="carousel_fadder"></div>
        </div>
        <div class="carousel_body">
          <div class="post_type_ico">
            <span class="post_type_video"></span>
          </div><!-- .post type -->
          <div class="carousel_title">
            <h6><a href="#">Lorem ipsum dolor</a></h6>
          </div>
          <div class="carousel_desc">
            <div class="exc">Consectetur adipiscing elit ac Praesent laoreet rutrum suada duis ac nisi, ac sapiens ccumsan elementum acnullam</div>
          </div>
          <div class="carousel_meta"><span>25 April 2019 in</span> <a href="#">Portraits,</a> <a href="#">Fashion</a></div>
        </div>
      </div>
    </li>

    <li>
      <div class="item">
        <div class="img_block">
          <img src="img/pictures/carousel_works5.jpg" alt="" width="270" height="190">
          <div class="ico_gallery"></div>
          <div class="carousel_fadder"></div>
        </div>
        <div class="carousel_body">
          <div class="post_type_ico">
            <span class="post_type_image"></span>
          </div><!-- .post type -->
          <div class="carousel_title">
            <h6><a href="#">Dolor sit amet</a></h6>
          </div>
          <div class="carousel_desc">
            <div class="exc">Nunc justo lacus, molestie ut porta id pharetra vestibulum proin sit amet aliquet massa phasellus id tristique mi</div>
          </div>
          <div class="carousel_meta"><span>25 April 2019 in</span> <a href="#">Portraits,</a> <a href="#">Fashion</a></div>
        </div>
      </div>
    </li>

    <li>
      <div class="item">
        <div class="img_block">
          <img src="img/pictures/carousel_works6.jpg" alt="" width="270" height="190">
          <div class="ico_gallery"></div>
          <div class="carousel_fadder"></div>
        </div>
        <div class="carousel_body">
          <div class="post_type_ico">
            <span class="post_type_gallery"></span>
          </div><!-- .post type -->
          <div class="carousel_title">
            <h6><a href="#">Nunc justo lacus</a></h6>
          </div>
          <div class="carousel_desc">
            <div class="exc">Consectetur adipiscing elit ac Praesent laoreet rutrum suada duis ac nisi, ac sapiens ccumsan elementum acnullam</div>
          </div>
          <div class="carousel_meta"><span>25 April 2019 in</span> <a href="#">Portraits,</a> <a href="#">Fashion</a></div>
        </div>
      </div>
    </li>
  </ul>
</div>
EOD;

  return $output;
}

/**
 * Theme function to build artwork slider.
 *
 * @param $images
 * @return string
 */
function theme_ashleyjan_custom_artwork_slider($images) {
  $output = array();
  $images = reset($images);
  $output[] = <<<EOD
<div class="featured_image_full">
    <div class="slider-wrapper theme-default">
        <div class="nivoSlider">
EOD;
  foreach ($images as $image) {
    $image_path = image_style_url('slidesquare_870x678', $image['uri']);
    $output[] = "<img src=\"$image_path\" data-thumb=\"$image_path\" alt=\"\" />";
  }
    $output[] = <<<EOD
        </div>
    </div><!-- .slider-wrapper  -->
</div><!-- .featured_image_full -->
EOD;

  return implode('', $output);

}

/**
 * Theme callback for a single portfolio item.
 *
 * @param $artwork
 * @return string
 */
function theme_ashleyjan_custom_portfolio_single($artwork) {
  $image_path = image_style_url('portfolio2_570x444', $artwork['image']);
  $title = $artwork['title'];
  $link = $artwork['link'];
  $category = $artwork['category'];
  $output[] = <<<EOD
    <div data-category="$category" class="$category element row-fluid">
        <div class="filter_img gallery_item">
EOD;
  $output[] = "<a href=\"$link\">";
  $output[] = "<img src=\"$image_path\" alt=\"$title\">";
  $output[] = "<div class=\"gallery_fadder\"></div>";
  $output[] = "<div class=\"ico_gallery\"></div><!-- .ico_gallery -->";
  $output[] = "<div class=\"gallery_descr\">";
  $output[] = "<h6 class=\"gallery_title\">$title</h6>";
  $output[] = "</div></a>";
  $output[] = "</div><!-- filter_img --></div><!-- portraits element -->";
  return implode('', $output);
}

/**
 * Theme callback for portfolio page with all items.
 * 
 * @param $artworks
 * @return string
 */
function theme_ashleyjan_custom_portfolio($artworks) {
  $categories = $artworks['categories'];
  $artworks = $artworks['artworks'];
  $output = array();
  $output[] = <<<EOD
<div class="filter_block">
    <div class="filter_navigation">
        <ul id="options" class="splitter">
            <li>
                <ul data-option-key="filter" class="optionset">
EOD;
  foreach ($categories as $category) {
    $uppercase = ucfirst($category);
    $output[] = "<li><a title=\"View all post filed under $uppercase\" data-option-value=\".$category\" href=\"#filter\">$uppercase</a></li>";
  }

  $output[] = <<<EOD
                  <li class="selected"><a data-option-value="*" href="#filter">All Works</a></li>
                </ul>
            </li>
        </ul>
    </div><!-- .filter_navigation -->
</div><!-- .filter_block -->

<div class="portfolio_block image-grid columns2" id="list">
EOD;
  $output[] = implode('', $artworks);
  $output[] = <<<EOD
</div><!-- .portfolio_block -->
EOD;

  return implode('', $output);
}

/**
 * Theme function to return latest post html.
 *
 * @param $post
 * @return string
 */
function theme_ashleyjan_custom_latest_artwork($post) {
  $image = $post['image'];
  $link = $post['link'];
  $title = $post['title'];
  $summary = $post['summary'];
  $output = <<<EOD
    <li>
      <div class="recent_posts_img">
        <a href="$link"><img src="$image" alt="" width="88" height="88"/></a>
      </div>
      <div class="recent_posts_content">
        <a href="$link" class="post_title">$title</a>
        $summary
      </div>
      <div class="clear"></div>
    </li>
EOD;
  return $output;
}