<?php
/**
 * @file
 * Featured Works plugin.
 */

class AshleyJanFeaturedWorksBean extends BeanPlugin {
  /**
   * Builds extra settings for the block edit form.
   */
  public function form($bean, $form, &$form_state) {}

  /**
   * Displays the bean.
   */
  public function view($bean, $content, $view_mode = 'default', $langcode = NULL) {
//    $query = new EntityFieldQuery();
//    $query
//      ->entityCondition('entity_type', 'node')
//      ->entityCondition('bundle', 'article')
//      ->propertyCondition('status', 1)
//      ->propertyOrderBy('created', 'DESC')
//      ->range(0, $bean->settings['records_shown']);
//    $result = $query->execute();
//    if (empty($result)) {
//      $content['nodes'] = array();
//    }
//    else {
//      foreach ($result['node'] as $node) {
//        $node = node_load($node->nid, $node->vid);
//        $content['nodes'][$node->nid] = node_view($node, $bean->settings['node_view_mode']);
//      }
//    }
//    $content['more_link']['#markup'] = theme('article_listing_more_link', array('text' => $bean->more_link['text'], 'path' => $bean->more_link['path']));
    $content[]['#markup'] = theme('ashleyjan_custom_featuredworks', array('works' => array()));
    return $content;
  }
}