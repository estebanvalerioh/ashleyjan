<?php
/**
 * @file
 * theme overrides etc for Frame theme
 */

/**
 * Implements template_preprocess_page().
 */
function frame_preprocess_page(&$vars) {
  $main_menu = menu_tree('main-menu');
  $vars['main_menu'] = drupal_render($main_menu);

  if (drupal_is_front_page()) {
    $vars['front_gallery'] = ashleyjan_custom_frontpage_gallery();
  }
  else {
    switch (current_path()) {
      case 'portfolio':
      case 'about':
      case 'node/1':
        $vars['right_sidebar'] = FALSE;
        $content_block = 'no-sidebar';
        $fl_container = $posts_block = 'span12';
        $module = 'module_portfolio';
        break;

      default:
        $vars['right_sidebar'] = TRUE;
        break;
    }

    $vars['content_block'] = isset($content_block) ? $content_block : 'right-sidebar';
    $vars['fl_container'] = isset($fl_container) ? $fl_container : 'span9';
    $vars['posts_block'] = isset($posts_block) ? $posts_block : 'span9';
    $vars['module'] = isset($module) ? $module : 'module_blog';
  }

  $vars['latest_artwork'] = ashleyjan_custom_latest_artwork();
}

/**
 * Implements template_preprocess_node().
 *
 * @param $vars
 */
function frame_preprocess_node(&$vars) {
  if (isset($vars['type']) && ($vars['type'] == 'artwork')) {
    $slideshow = ashleyjan_custom_artwork_slideshow();
    $vars['content']['field_artwork_image'] = array(
      '#markup' => $slideshow,
    );
  }
}

/**
 * Implements theme_menu_tree__MENU_NAME
 *
 * @param $variables
 * @return string
 */
function frame_menu_tree__main_menu(&$variables) {
    return '<nav><ul class="menu">' . $variables['tree'] . '</ul></nav>';
}

/**
 * Implements theme_menu_link__MENU_NAME
 *
 * @param $variables
 * @return string
 */
function frame_menu_link__main_menu(&$variables) {
  $element = $variables['element'];
  $sub_menu = '';

  $leaf = array('leaf', 'expanded');
  $element['#attributes']['class'] = array_diff($element['#attributes']['class'], $leaf);

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

/**
 * Implements theme_breadcrumb
 *
 * @param $variables
 * @return string
 */
function frame_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  $breadcrumb[] = '<span>' . drupal_get_title() . '</span>';

  $crumbs = '';
  if (!empty($breadcrumb)) {
    foreach($breadcrumb as $value) {
      $crumbs .= $value;
    }
  }
  return $crumbs;
}
