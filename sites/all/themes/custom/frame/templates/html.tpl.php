<?php
/**
 * @file
 * Frame to display the basic html structure of a single
 * Drupal page.
 */
?>
<!DOCTYPE html>
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en" class="ie ie9"> <!--<![endif]-->

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800,400italic,600italic,700italic' rel='stylesheet' type='text/css'>
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  <!--[if IE 8 ]>
  <link rel="stylesheet" type="text/css" href="css/ie_css.css">
  <script>
    var e = ("article,aside,figcaption,figure,footer,header,hgroup,nav,section,time").split(',');
    for (var i = 0; i < e.length; i++) {
      document.createElement(e[i]);
    }
  </script>
  <![endif]-->
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
<?php print $page_top; ?>
<?php print $page; ?>
<?php print $scripts; ?>
<?php print $page_bottom; ?>
<div class="header2top"></div>
</body>
</html>
